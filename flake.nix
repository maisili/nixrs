{
  description = "nixrs";

  outputs = { self }:
  {
    datom = { rustMod =
      {
        nightly = true;
        indeksCrates = {
          async-std = { features = [  "default" "attributes" ]; };
          serde_json = {};
          serde = { features = [ "derive" "default" ]; };
        };

      };

    };
  };
}
