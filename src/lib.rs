#![crate_name = "nixrs"]
#![crate_type = "lib"]

use {
  async_std::{
    fs::read,
    io::Result,
    path::Path,
  },
  serde_json::Value,
};

pub async fn get_attrs() -> Result<Value> {
  let iuniks: &Path = Path::new("./.attrs.json");
  let json_bofyr: Vec<u8> = read(iuniks).await.unwrap();
  let slice: &[u8] = json_bofyr.as_slice();
  let value: Value = serde_json::from_slice(slice).unwrap();
  Result::Ok(value)
}
